#!/bin/bash

# systemctl -a | grep can 
#
#sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.1-3\x2d4.3.1.1-3\x2d4.3.1.1:1.0-net-can0.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.1-3\x2d4.3.1.1-3\x2d4.3.1.1:1.0-net-can1.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.1-3\x2d4.3.1.2-3\x2d4.3.1.2:1.0-net-can2.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.1-3\x2d4.3.1.2-3\x2d4.3.1.2:1.0-net-can3.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.1-3\x2d4.3.1.3-3\x2d4.3.1.3:1.0-net-can6.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.1-3\x2d4.3.1.3-3\x2d4.3.1.3:1.0-net-can7.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.1-3\x2d4.3.1.4-3\x2d4.3.1.4:1.0-net-can10.device loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.1-3\x2d4.3.1.4-3\x2d4.3.1.4:1.0-net-can11.device loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.3-3\x2d4.3.3.1-3\x2d4.3.3.1:1.0-net-can4.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.3-3\x2d4.3.3.1-3\x2d4.3.3.1:1.0-net-can5.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.3-3\x2d4.3.3.2-3\x2d4.3.3.2:1.0-net-can8.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.3-3\x2d4.3.3.2-3\x2d4.3.3.2:1.0-net-can9.device  loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.3-3\x2d4.3.3.3-3\x2d4.3.3.3:1.0-net-can12.device loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.3-3\x2d4.3.3.3-3\x2d4.3.3.3:1.0-net-can13.device loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.3-3\x2d4.3.3.4-3\x2d4.3.3.4:1.0-net-can14.device loaded    active     plugged   USB-CANmodul8
#  sys-devices-pci0000:00-0000:00:14.0-usb3-3\x2d4-3\x2d4.3-3\x2d4.3.3-3\x2d4.3.3.4-3\x2d4.3.3.4:1.0-net-can15.device loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can0.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can1.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can10.device                                                                             loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can11.device                                                                             loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can12.device                                                                             loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can13.device                                                                             loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can14.device                                                                             loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can15.device                                                                             loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can2.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can3.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can4.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can5.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can6.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can7.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can8.device                                                                              loaded    active     plugged   USB-CANmodul8
#  sys-subsystem-net-devices-can9.device                                                                              loaded    active     plugged   USB-CANmodul8
#  iscsi.service                                                                                                      loaded    inactive   dead      Login and scanning of iSCSI devices
# lvm2-pvscan@8:18.service                                                                                           loaded    active     exited    LVM2 PV scan on device 8:18
#  wpa_supplicant.service                                                                                             loaded    active     running   WPA Supplicant daemon
#  system-lvm2\x2dpvscan.slice                                                                                        loaded    active     active    system-lvm2\x2dpvscan.slice
#
#
# init one systec16 which is the only USB CAN device, no peak
for n in 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15; do

echo $n
echo "ip link set can$n down"
ip link set can$n down
echo "ip link set can$n type can bitrate 125000"
ip link set can$n type can bitrate 125000
echo "ip link set can$n up"
ip link set can$n up
ip link show can$n

done

